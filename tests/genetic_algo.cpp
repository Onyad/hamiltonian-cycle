#include <catch2/catch_all.hpp>
#include <algo/genetic.hpp>
#include <graph/graph.hpp>
#include <check/check_path.hpp>

using graph::Graph;

TEST_CASE("simple check", "[genetic_algo]") {
  {
    Graph<> g(4);
    g.AddEdge(0, 1);
    g.AddEdge(1, 2);
    g.AddEdge(2, 3);

    REQUIRE(algo::genetic::GetHamiltonialPath(g));

    g.AddEdge(0, 3);

    REQUIRE(algo::genetic::GetHamiltonialPath(g));

    g.AddEdge(0, 2);

    REQUIRE(algo::genetic::GetHamiltonialPath(g));
  }

  auto check_path = [](const Graph<>& g) {
    auto opath = algo::genetic::GetHamiltonialPath(g);
    REQUIRE(opath);
    auto path = *opath;
    REQUIRE(check::CheckPath(g, path));
  };

  {
    Graph<> g(4);

    g.AddEdge(0, 1);
    g.AddEdge(0, 2);
    g.AddEdge(0, 3);

    REQUIRE(!algo::genetic::GetHamiltonialPath(g));

    g.AddEdge(1, 2);

    check_path(g);

    g.AddEdge(2, 3);

    check_path(g);
  }
}
