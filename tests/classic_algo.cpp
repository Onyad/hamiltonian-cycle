#include <catch2/catch_all.hpp>
#include <algo/classic.hpp>
#include <check/check_path.hpp>
#include <graph/gens/uniform.hpp>

#include <iostream>

using graph::Graph;

TEST_CASE("simple check", "[classic_algo]") {
  {
    Graph<> g(4);
    g.AddEdge(0, 1);
    g.AddEdge(1, 2);
    g.AddEdge(2, 3);

    REQUIRE(algo::classic::CheckHamiltonialPath(g));

    g.AddEdge(0, 3);

    REQUIRE(algo::classic::CheckHamiltonialPath(g));

    g.AddEdge(0, 2);

    REQUIRE(algo::classic::CheckHamiltonialPath(g));
  }
  {
    Graph g(4);
    g.AddEdge(0, 1);
    g.AddEdge(0, 2);
    g.AddEdge(0, 3);

    REQUIRE(!algo::classic::CheckHamiltonialPath(g));

    g.AddEdge(1, 2);

    REQUIRE(algo::classic::CheckHamiltonialPath(g));

    g.AddEdge(2, 3);

    REQUIRE(algo::classic::CheckHamiltonialPath(g));
  }
}

TEST_CASE("simple get", "[classic_algo]") {
  auto check_path = [](const Graph<>& g) {
    auto opath = algo::classic::GetHamiltonialPath(g);
    REQUIRE(opath);
    auto path = *opath;
    REQUIRE(check::CheckPath(g, path));
  };

  {
    Graph g(4);
    g.AddEdge(0, 1);
    g.AddEdge(1, 2);
    g.AddEdge(2, 3);

    REQUIRE(algo::classic::GetHamiltonialPath(g));

    g.AddEdge(0, 3);

    check_path(g);

    g.AddEdge(0, 2);

    check_path(g);
  }
  {
    Graph g(4);
    g.AddEdge(0, 1);
    g.AddEdge(0, 2);
    g.AddEdge(0, 3);

    REQUIRE(!algo::classic::GetHamiltonialPath(g));

    g.AddEdge(1, 2);

    REQUIRE(algo::classic::GetHamiltonialPath(g));

    g.AddEdge(2, 3);

    check_path(g);
  }
}

TEST_CASE("Stress test", "[classic_algo]") {
  using namespace graph::gens;

  Uniform gen(0.6);
  std::mt19937_64 rnd(42);

  const size_t count_iterations = 100;
  auto ns = {4, 8, 11, 13};

  for (size_t i = 0; i < count_iterations; ++i) {
    for (auto n : ns) {
      auto g = gen.Gen(rnd, n);

      auto opath = algo::classic::GetHamiltonialPath(g);
      if (!opath) {
        continue;
      }

      REQUIRE(check::CheckPath(g, *opath));
    }
  }
}
