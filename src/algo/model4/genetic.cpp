#include <algo/model4/genetic.hpp>

#include <cmath>
#include <cstdlib>
#include <iostream>
#include <set>
#include <list>

namespace algo::model4::genetic {
using namespace graph;

using Path = std::vector<size_t>;
using Wgraph = Graph<double, size_t>;

uint32_t rnd() {
  return rand();
}

double drnd() {
  return double(rand()) / RAND_MAX;
}

double f(size_t n) {
  return std::log(n + std::exp(1));
}

void CountWgraph(Wgraph& g) {
  for (auto v : g) {
    *v.info() = 0;

    for (auto u : v) {
      *v.info() += f(*u.info());
    }
  }
}

Path GeneratePath(const Wgraph& g) {
  const size_t n = g.size();

  Path p;
  p.reserve(n);

  p.push_back(rnd() % n);
  auto v = g[p.back()];

  std::set<std::pair<int, int>> s;

  while (p.size() != n) {
    double r = drnd() * (*v.info());
    for (auto e : v) {
      double q = f(*e.info());
      if (r <= q) {
        if (s.count({e.from().get(), e.to().get()}) == 1) {
          if ((rnd() % 5) != 0) {
            break;
          }
        }

        v = e.to();
        p.push_back(v.get());

        s.emplace(e.from().get(), e.to().get());
        s.emplace(e.to().get(), e.from().get());

        break;
      }
      r -= q;
    }
  }

  return p;
}

size_t CountUnique(Path path) {
  std::sort(path.begin(), path.end());
  return std::unique(path.begin(), path.end()) - path.begin();
}

template <typename T>
std::vector<size_t> ArgMaxes(const std::vector<T>& v, size_t count) {
  const size_t n = v.size();
  std::set<std::pair<T, size_t>> s;  // TODO maybe replace to heap with
                                     // std::vector

  for (size_t i = 0; i < n; ++i) {
    s.emplace(v[i], i);
    if (s.size() > count) {
      s.erase(std::prev(s.end()));
    }
  }

  std::vector<size_t> result;
  for (auto& i : s) {
    result.push_back(i.second);
  }

  return result;
}

std::optional<std::vector<size_t>> GetHamiltonialPath(const Graph<>& g,
                                                      std::function<bool(size_t)> check) {
  const size_t n = g.size();
  Wgraph w(n);

  for (auto v : w) {
    v.info() = std::make_shared<double>(0);
  }

  for (auto v : g) {
    for (auto u : v) {
      if (!w.CheckEdge(v.get(), u.to().get())) {
        w.AddEdge(v.get(), u.to().get(), std::make_shared<size_t>(0));
      }
    }
  }

  std::vector<Path> pathes;

  const size_t count_pathes = 5000;
  const size_t count_best_pathes = 500;

  for (size_t it = 0; check(n); ++it) {
    pathes.clear();
    CountWgraph(w);

    for (size_t i = 0; i < count_pathes; ++i) {
      pathes.push_back(GeneratePath(w));
    }

    std::vector<size_t> oracle(count_pathes);
    for (size_t i = 0; i < count_pathes; ++i) {
      oracle[i] = CountUnique(pathes[i]);

      if (oracle[i] == n) {
        std::vector<size_t> v(pathes[i].begin(), pathes[i].end());

        return v;
      }
    }

    auto arg_maxes = ArgMaxes(oracle, count_best_pathes);

    for (auto ind : arg_maxes) {
      auto& path = pathes[ind];
      for (auto it = std::next(path.begin()); it != path.end(); ++it) {
        *w.GetEdgeInfo(*std::prev(it), *it) += 1;
      }
    }
  }

  return {};
}
}  // namespace algo::model4::genetic
