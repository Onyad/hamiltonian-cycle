#pragma once
#include <graph/graph.hpp>
#include <optional>
#include <functional>

namespace algo::model3::genetic {
std::optional<std::vector<size_t>> GetHamiltonialPath(const graph::Graph<>& graph, std::function<bool(size_t)>);
}  // namespace algo::genetic
