#include <algo/classic.hpp>

namespace algo::classic {
using namespace graph;

bool CheckHamiltonialPath(const graph::Graph<>& graph) {
  const size_t n = graph.size();
  const size_t end_mask = 1 << n;

  if (n > 25) {
    throw std::runtime_error("do calc too long time");
  }

  using Bool = uint8_t;
  std::vector<std::vector<Bool>> dp(n, std::vector<Bool>(1 << n));
  for (size_t i = 0; i < n; ++i) {
    dp[i][1 << i] = true;
  }

  for (uint32_t mask = 1; mask < end_mask; ++mask) {
    for (size_t u = 0; u < n; ++u) {
      if (!dp[u][mask]) {
        continue;
      }
      for (auto v : graph[u]) {
        if (!((mask >> v.to().get()) & 1)) {
          dp[v.to().get()][mask | (1 << v.to().get())] = true;
        }
      }
    }
  }

  for (size_t v = 0; v < n; ++v) {
    if (dp[v][end_mask - 1]) {
      return true;
    }
  }

  return false;
}

std::optional<std::vector<size_t>> GetHamiltonialPath(const graph::Graph<>& graph) {
  const size_t n = graph.size();
  const size_t end_mask = 1 << n;

  if (n > 25) {
    throw std::runtime_error("do calc too long time");
  }

  using Bool = uint8_t;
  std::vector<std::vector<Bool>> dp(n, std::vector<Bool>(1 << n));
  std::vector<std::vector<size_t>> back(n, std::vector<size_t>(1 << n));

  for (size_t i = 0; i < n; ++i) {
    dp[i][1 << i] = true;
  }

  for (uint32_t mask = 1; mask < end_mask; ++mask) {
    for (size_t u = 0; u < n; ++u) {
      if (!dp[u][mask]) {
        continue;
      }
      for (auto v : graph[u]) {
        if (!((mask >> v.to().get()) & 1)) {
          dp[v.to().get()][mask | (1 << v.to().get())] = true;
          back[v.to().get()][mask | (1 << v.to().get())] = u;
        }
      }
    }
  }

  for (size_t v = 0; v < n; ++v) {
    if (dp[v][end_mask - 1]) {
      std::vector<size_t> result;

      size_t cur_v = v;
      size_t cur_mask = end_mask - 1;

      // result.push_back(0);

      while (result.size() != n) {
        result.push_back(cur_v);
        size_t save_v = cur_v;
        cur_v = back[cur_v][cur_mask];
        cur_mask &= ~(1 << save_v);
      }

      // std::cout << "result size: " << result.size() << std::endl;
      // std::cout << "n: " << n << std::endl;

      return result;
    }
  }

  return {};
}

}  // namespace algo::classic
