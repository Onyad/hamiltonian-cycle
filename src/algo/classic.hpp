#pragma once
#include <graph/graph.hpp>
#include <optional>

namespace algo::classic {
bool CheckHamiltonialPath(const graph::Graph<>& graph);
std::optional<std::vector<size_t>> GetHamiltonialPath(const graph::Graph<>& graph);
}  // namespace algo::classic
