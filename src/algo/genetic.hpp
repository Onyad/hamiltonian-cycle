#pragma once
#include <graph/graph.hpp>
#include <optional>

namespace algo::genetic {
std::optional<std::vector<size_t>> GetHamiltonialPath(const graph::Graph<>& graph, size_t limit = 10);
}  // namespace algo::genetic
