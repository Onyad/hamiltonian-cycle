#include <algo/genetic.hpp>

#include <iostream>
#include <set>
#include <list>

namespace algo::genetic {
using namespace graph;

using Path = std::list<size_t>;
using Wgraph = Graph<size_t, size_t>;

uint32_t rnd() {
  return rand();
}

void CountWgraph(Wgraph& g) {
  for (auto v : g) {
    *v.info() = 0;

    for (auto u : v) {
      *v.info() += *u.info() + 1;
    }
  }
}

Path GeneratePath(const Wgraph& g) {
  const size_t n = g.size();

  Path p;
  // p.reserve(n);

  p.push_back(rnd() % n);
  auto v = g[p.back()];

  std::set<std::pair<int, int>> s;

  while (p.size() != n) {
    auto front = g[p.front()];
    uint32_t r = rnd() % (*front.info() + *v.info());
    if (r < *v.info()) {
      for (auto e : v) {
        if (s.count({e.from().get(), e.to().get()}) == 0 && r <= *e.info()) {
          v = e.to();
          p.push_back(v.get());

          s.emplace(e.from().get(), e.to().get());
          s.emplace(e.to().get(), e.from().get());

          break;
        }
        r -= *e.info() + 1;
      }
    } else {
      r -= *v.info();
      for (auto e : front) {
        if (s.count({e.from().get(), e.to().get()}) == 0 && r <= *e.info()) {
          // v = e.to();
          p.push_front(e.to().get());

          s.emplace(e.from().get(), e.to().get());
          s.emplace(e.to().get(), e.from().get());

          break;
        }
        r -= *e.info() + 1;
      }
    }
  }

  return p;
}

size_t CountUnique(Path path) {
  path.sort();
  return std::distance(path.begin(), std::unique(path.begin(), path.end()));
  // std::sort(path.begin(), path.end());
  // return std::unique(path.begin(), path.end()) - path.begin();
}

template <typename T>
std::vector<size_t> ArgMaxes(const std::vector<T>& v, size_t count) {
  const size_t n = v.size();
  std::set<std::pair<T, size_t>> s;  // TODO maybe replace to heap with
                                     // std::vector

  for (size_t i = 0; i < n; ++i) {
    s.emplace(v[i], i);
    if (s.size() > count) {
      s.erase(std::prev(s.end()));
    }
  }

  std::vector<size_t> result;
  for (auto& i : s) {
    result.push_back(i.second);
  }

  return result;
}

std::optional<std::vector<size_t>> GetHamiltonialPath(const Graph<>& g, size_t limit) {
  const size_t n = g.size();
  Wgraph w(n);

  for (auto v : w) {
    v.info() = std::make_shared<size_t>(0);
  }

  for (auto v : g) {
    for (auto u : v) {
      if (!w.CheckEdge(v.get(), u.to().get())) {
        w.AddEdge(v.get(), u.to().get(), std::make_shared<size_t>(0));
      }
    }
  }

  std::vector<Path> pathes;

  const size_t count_pathes = 5000;
  const size_t count_best_pathes = 1000;

  for (size_t it = 0; it < n * limit; ++it) {
    pathes.clear();
    CountWgraph(w);

    for (size_t i = 0; i < count_pathes; ++i) {
      pathes.push_back(GeneratePath(w));
    }

    std::vector<size_t> oracle(count_pathes);
    for (size_t i = 0; i < count_pathes; ++i) {
      oracle[i] = CountUnique(pathes[i]);

      if (oracle[i] == n) {
        std::vector<size_t> v(pathes[i].begin(), pathes[i].end());
        return v;
      }
    }

    auto arg_maxes = ArgMaxes(oracle, count_best_pathes);

    for (auto ind : arg_maxes) {
      auto& path = pathes[ind];
      for (auto v : path) {
        (*w[v].info()) += oracle[ind];
      }
    }
  }

  return {};
}
}  // namespace algo::genetic
