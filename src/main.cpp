#include <chrono>
#include <iostream>
#include <iomanip>
#include <fstream>

#include <rang.hpp>
#include <graph/gens/uniform.hpp>
#include <algo/genetic.hpp>
#include <algo/model1/genetic.hpp>
#include <algo/model2/genetic.hpp>
#include <algo/model3/genetic.hpp>
#include <algo/model4/genetic.hpp>
#include "algo/classic.hpp"

int main() {
  using std::cout;
  using std::endl;
  using namespace rang;

  cout << fg::green << "hello world" << fg::reset << endl;

  // const size_t n = 300;
  // graph::Graph g(n);

  std::mt19937_64 rnd(42);

  const size_t count_repeat = 4;

  std::ofstream csv("genetic_model4.csv" /*, std::ios::app*/);
  csv << std::setprecision(9) << std::fixed;
  csv << "count,p,time" << std::endl;

  std::cout << std::setprecision(9) << std::fixed;
  for (size_t n = 5; n < 22; n++) {
    for (double u = 0.5; u < 1; u += 0.2) {
      for (size_t repeat = 0; repeat < count_repeat; ++repeat) {
        auto g = graph::gens::Uniform(u).Gen(rnd, n);

        for (size_t i = 0; i + 1 < n; ++i) {
          g.AddEdge(i, i + 1);
        }

        if constexpr (false) {
          auto start = std::chrono::system_clock::now();
          algo::classic::GetHamiltonialPath(g);
          auto end = std::chrono::system_clock::now();
          std::chrono::duration<double> s = end - start;
          std::cout << "classic n:" << n << " u:" << u << " - " << s.count() << std::endl;
          csv << n << "," << u << "," << s.count() << std::endl;
        }
        if constexpr (true) {
          auto start = std::chrono::system_clock::now();
          auto check = [start](size_t n) {
            auto end = std::chrono::system_clock::now();
            std::chrono::duration<double> s = end - start;
            double res = s.count();
            return res < 3;
          };
          auto o = algo::model4::genetic::GetHamiltonialPath(g, check);

          auto end = std::chrono::system_clock::now();
          std::chrono::duration<double> s = end - start;

          double res = s.count();

          if (!o) {
            res = 5;
          }
          std::cout << "genetic n:" << n << " u:" << u << " - " << res << std::endl;
          csv << n << "," << u << "," << res << std::endl;
        }
      }
    }
  }
}
