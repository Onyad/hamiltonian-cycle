#pragma once
#include <graph/graph.hpp>

namespace check {
bool CheckPath(const graph::Graph<>& graph, const std::vector<size_t>& cicle);
}  // namespace check
