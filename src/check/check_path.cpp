#include <check/check_path.hpp>

namespace check {
bool CheckPath(const graph::Graph<>& graph, const std::vector<size_t>& cicle) {
  const size_t n = graph.size();
  if (n != cicle.size()) {
    return false;
  }
  if (cicle.size() <= 1) {
    return true;
  }
  for (size_t i = 0; i + 1 < n; ++i) {
    graph.CheckEdge(cicle[i], cicle[i + 1]);
  }

  // std::cout << "alskdfjklasjdf" << std::endl;
  // for (auto i: cicle) {
  //   std::cout << i << " ";
  // }
  // std::cout << std::endl;
  auto copy = cicle;

  std::sort(copy.begin(), copy.end());
  size_t count = std::unique(copy.begin(), copy.end()) - copy.begin();
  // std::cout << count << " " << n << std::endl;

  return count == n;
}
}
