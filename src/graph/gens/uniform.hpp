#pragma once
#include <graph/graph.hpp>
#include <random>

namespace graph::gens {
class Uniform {
 public:
  Uniform(double p) : p_(p) {
    if (p > 1) {
      throw std::runtime_error("Error construct uniform");
    }
  }

  Graph<> Gen(std::mt19937_64& rnd, size_t n) {
    const uint64_t delim = rnd.max() * p_;

    Graph result(n);

    for (size_t u = 0; u < n; ++u) {
      for (size_t v = 0; v < u; ++v) {
        if (rnd() < delim) {
          result.AddEdge(u, v);
        }
      }
    }

    return result;
  }

 private:
  double p_;
};
}  // namespace graph::gens
