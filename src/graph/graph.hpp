#pragma once
#include <cctype>
#include <vector>
#include <utils.hpp>
#include <iostream>

namespace graph {

template <class VertexInfo = utils::Empty, class EdgeInfo = utils::Empty>
class Graph {
 public:
  class EdgeIterator;

  class Vertex {
    friend EdgeIterator;
    friend Graph;

   public:
    Vertex() = default;

    Vertex& operator++() {
      ++v_;
      return *this;
    }

    Vertex operator++(int) {
      ++v_;
      return Vertex{graph_, v_ - 1};
    }

    Vertex operator*() const {
      return *this;
    }

    EdgeIterator begin() const {
      return EdgeIterator{*this, 0};
    }

    EdgeIterator end() const {
      return EdgeIterator{*this, graph_->edges_from_vertex_[v_].size()};
    }

    bool CheckEdge(size_t u) const {
      graph_->CheckEdge(v_, u);
    }

    bool operator==(const Vertex& another) const {
      return (graph_ == another.graph_) && (v_ == another.v_);
    }

    bool operator!=(const Vertex& another) const {
      return !(*this == another);
    }

    size_t get() const {
      return v_;
    }

    std::shared_ptr<VertexInfo>& info() {
      return graph_->vertexes_info_[v_];
    }

    std::shared_ptr<const VertexInfo> info() const {
      return graph_->vertexes_info_[v_];
    }

    size_t operator[](size_t index) const {
      return graph_->edges_from_vertex_[v_][index];
    }

   private:
    Vertex(Graph* graph, size_t v) : graph_(graph), v_(v) {}

    Graph* graph_{nullptr};
    size_t v_{0};
  };

  struct EdgeIterator {
    friend Vertex;

   public:
    bool operator==(const EdgeIterator& another) {
      return (e_ == another.e_) && (v_ == another.v_);
    }

    bool operator!=(const EdgeIterator& another) {
      return !(*this == another);
    }

    Vertex from() const {
      return v_;
    }

    Vertex to() const {
      return Vertex{v_.graph_, v_.graph_->edges_from_vertex_[from().get()][e_]};
    }

    std::shared_ptr<EdgeInfo>& info() {
      return v_.graph_->edges_info_[from().get()][e_];
    }

    std::shared_ptr<const EdgeInfo> info() const {
      return v_.graph_->edges_info_[to()][e_];
    }

    EdgeIterator& operator++() {
      ++e_;
      return *this;
    }

    EdgeIterator operator++(int) {
      ++e_;
      return EdgeIterator{v_, e_ - 1};
    }

    EdgeIterator operator*() const {
      return *this;
    }

   private:
    EdgeIterator(Vertex v, size_t e) : v_(v), e_(e) {}

    Vertex v_;
    size_t e_;
  };

  Graph(size_t n)
      : n_(n), edges_from_vertex_(n), edges_(n, std::vector<size_t>(n, 0)), vertexes_info_(n), edges_info_(n) {}

  const Vertex begin() const {
    return Vertex{const_cast<Graph*>(this), 0};
  }

  const Vertex end() const {
    return Vertex{const_cast<Graph*>(this), n_};
  }

  Vertex begin() {
    return Vertex{this, 0};
  }

  Vertex end() {
    return Vertex{this, n_};
  }

  Vertex operator[](size_t n) {
    return Vertex{this, n};
  }

  const Vertex operator[](size_t n) const {
    return Vertex{const_cast<Graph*>(this), n};
  }

  void AddEdge(size_t u, size_t v, std::shared_ptr<EdgeInfo> info = nullptr) {
    if (edges_[v][u] != 0) {
      return;
    }

    edges_from_vertex_[u].push_back(v);
    edges_from_vertex_[v].push_back(u);

    edges_info_[u].push_back(info);
    edges_info_[v].push_back(info);

    edges_[v][u] = edges_from_vertex_[v].size();
    edges_[u][v] = edges_from_vertex_[u].size();
  }

  bool CheckEdge(size_t u, size_t v) const {
    return edges_[u][v] != 0;
  }

  size_t size() const {
    return n_;
  }

  auto GetEdgeInfo(size_t u, size_t v) {
    return edges_info_[u][edges_[u][v] - 1];
  }

 private:
  friend EdgeIterator;
  friend Vertex;

  size_t n_;

  std::vector<std::vector<size_t>> edges_from_vertex_;
  std::vector<std::vector<size_t>> edges_;

  std::vector<std::shared_ptr<VertexInfo>> vertexes_info_;
  std::vector<std::vector<std::shared_ptr<EdgeInfo>>> edges_info_;
};
}  // namespace graph
